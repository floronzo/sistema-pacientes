import {NgModule} from '@angular/core';
import {AltaUsuariosComponent} from './alta-usuarios/alta-usuarios.component';
import {ModificacionUsuariosComponent} from './modificacion-usuarios/modificacion-usuarios.component';
import {BajaUsuariosComponent} from './baja-usuarios/baja-usuarios.component';
import {BuscarUsuariosComponent} from './buscar-usuarios/buscar-usuarios.component';
import {GlobalSharedModule} from '../global-shared/global-shared.module';

@NgModule({
  declarations: [
    AltaUsuariosComponent,
    BajaUsuariosComponent,
    ModificacionUsuariosComponent,
    BuscarUsuariosComponent,
  ],
  exports: [
    AltaUsuariosComponent,
    BajaUsuariosComponent,
    ModificacionUsuariosComponent,
    BuscarUsuariosComponent,
  ],
  imports: [
    GlobalSharedModule
  ]
})
export class UsuariosModule { }
