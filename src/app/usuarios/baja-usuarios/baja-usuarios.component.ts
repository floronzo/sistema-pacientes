import {Component, OnInit} from '@angular/core';
import {UsuarioResponse} from '../../usuario-response';
import {FormBuilder, Validators} from '@angular/forms';
import {UserService} from '../../user.service';
import {Router} from '@angular/router';
import {FormUtilsService} from '../../utils/form-utils.service';
import {UsuarioRequest} from '../../usuario-request';
import {SelectionModel} from '@angular/cdk/collections';

const initialSelection = [];
const allowMultiSelect = false;

@Component({
  selector: 'app-baja-usuarios',
  templateUrl: './baja-usuarios.component.html',
  styleUrls: ['./baja-usuarios.component.css']
})
export class BajaUsuariosComponent implements OnInit {

  foundUsers: UsuarioResponse[] = [];
  displayedColumns = ['select', 'nombre_usuario', 'dni_persona', 'id_perfil', 'telefono', 'mail'];
  userList: UsuarioResponse[];
  selection = new SelectionModel<UsuarioResponse>(allowMultiSelect, initialSelection);

  searchUserForm = this._formBuilder.group({
      nombre_usuario: ['',
        [
          Validators.minLength(4)
        ]
      ],
      dni_persona: ['',
        [
          Validators.pattern('^[0-9]*$'),
        ]
      ],
    }
  );

  constructor(private _formBuilder: FormBuilder,
              private _userService: UserService,
              private _router: Router,
              public _formUtils: FormUtilsService) {
  }

  ngOnInit(): void {
  }

  searchUser(): void {

    this.foundUsers = [];
    this._userService.searchUser(new UsuarioRequest(this.searchUserForm.value))
      .subscribe(response => {
        this.foundUsers.push(...response);
        this.userList = this.foundUsers;
      });
  }

  deleteUser(): void {

    if (this.selection.selected != null && this.selection.selected.length > 0) {
      this._userService.deleteUser(this.selection.selected[0])
        .then(response => {
          // Removes user from array
          this.userList = this.foundUsers.filter(user => user.dni_persona !== this.selection.selected[0].dni_persona);
          this.selection = new SelectionModel<UsuarioResponse>(allowMultiSelect, initialSelection);
        });
    }
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.foundUsers.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle(): void {
    this.isAllSelected() ?
      this.selection.clear() :
      this.foundUsers.forEach(row => this.selection.select(row));
  }

  resetTable(): void {
    this.userList = null;
    this.foundUsers = [];
  }
}
