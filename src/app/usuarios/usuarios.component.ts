import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  selectedOption: string;
  availableOptions: string[] = ['alta', 'baja', 'modificación', 'buscar'];

  constructor() { }

  ngOnInit(): void {
  }

  selectOption(option: string): void {
    this.selectedOption = option;
  }

  isSelectedOption(option: string): boolean {
    return this.selectedOption === option;
  }
}
