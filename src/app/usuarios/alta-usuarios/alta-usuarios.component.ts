import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {equalValueValidator} from '../../utils/equal-value-validator';
import {UserService} from '../../user.service';
import {Router} from '@angular/router';
import {UsuarioRequest} from '../../usuario-request';
import {UsuarioResponse} from '../../usuario-response';
import {PerfilResponse} from '../../perfil-response';
import {FormUtilsService} from '../../utils/form-utils.service';
import {CiudadResponse} from '../../ciudad-response';

@Component({
  selector: 'app-alta-usuarios',
  templateUrl: './alta-usuarios.component.html',
  styleUrls: ['./alta-usuarios.component.css']
})
export class AltaUsuariosComponent implements OnInit {

  user: UsuarioResponse;

  cities: CiudadResponse[] = [
    {
      nombre_ciudad: 'Rawson',
      id_ciudad: 100
    },
    {
      nombre_ciudad: 'Trelew',
      id_ciudad: 200
    },
    {
      nombre_ciudad: 'Puerto Madryn',
      id_ciudad: 300
    },
    {
      nombre_ciudad: 'Comodoro Rivadavia',
      id_ciudad: 400
    }
  ];

  profiles: PerfilResponse[] = [
    {
      nombre: 'Administrador',
      id_perfil: 10
    },
    {
      nombre: 'Director',
      id_perfil: 20
    },
    {
      nombre: 'Administrativo',
      id_perfil: 30
    },
    {
      nombre: 'Visitante',
      id_perfil: 40
    }
  ];

  registrationForm = this._formBuilder.group({
      nombre_usuario: ['',
        [
          Validators.required,
          Validators.minLength(4)
        ]
      ],
      clave_usuario: ['',
        [
          Validators.required,
          Validators.minLength(4)
        ]
      ],
      confirm_password: ['', Validators.required],
      dni_persona: ['',
        [
          Validators.required,
          Validators.pattern('^[0-9]*$'),
          Validators.minLength(4)
        ]
      ],
      id_perfil: ['',
        [Validators.required]
      ],
      id_ciudad: ['',
        [Validators.required]
      ],
      nombre: ['',
        [Validators.required]
      ],
      apellido: ['',
        [Validators.required]
      ],
      direccion: ['',
        [Validators.required]
      ],
      mail: ['', [
        Validators.required,
        Validators.pattern('^.+@.+$')]
      ],
      telefono: ['',
        [
          Validators.required,
          Validators.minLength(8)
        ]
      ],
      fecha_nac: ['',
        [
          Validators.required,
          Validators.pattern(/^\d{2}\/\d{2}\/\d{4}$/)
        ]
      ],
    },
    {validator: equalValueValidator('confirm_password', 'clave_usuario')}
  );

  constructor(private _formBuilder: FormBuilder,
              private _userService: UserService,
              private _router: Router,
              public _formUtils: FormUtilsService) {
  }

  ngOnInit(): void {
  }

  registerUser(): void {
    if (this.registrationForm.valid && this.registrationForm.touched) {
      this._userService.registerUser(new UsuarioRequest(this.registrationForm.value))
        .then(res => this._router.navigateByUrl('/landing'));
    }
  }

  selectProfile(profileId): void {
    this.registrationForm.controls.id_perfil.setValue(profileId.target.value);
  }

  selectCity(cityId): void {
    this.registrationForm.controls.id_perfil.setValue(cityId.target.value);
  }

}
