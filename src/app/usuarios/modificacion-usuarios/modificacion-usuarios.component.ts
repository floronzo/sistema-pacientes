import {Component, OnInit} from '@angular/core';
import {UsuarioResponse} from '../../usuario-response';
import {SelectionModel} from '@angular/cdk/collections';
import {FormBuilder, Validators} from '@angular/forms';
import {UserService} from '../../user.service';
import {Router} from '@angular/router';
import {FormUtilsService} from '../../utils/form-utils.service';
import {UsuarioRequest} from '../../usuario-request';
import {PerfilResponse} from '../../perfil-response';

const initialSelection = [];
const allowMultiSelect = false;

@Component({
  selector: 'app-modificacion-usuarios',
  templateUrl: './modificacion-usuarios.component.html',
  styleUrls: ['./modificacion-usuarios.component.css']
})
export class ModificacionUsuariosComponent implements OnInit {

  foundUsers: UsuarioResponse[] = [];
  displayedColumns = ['select', 'nombre_usuario', 'dni_persona', 'id_perfil', 'telefono', 'mail'];
  userList: UsuarioResponse[];
  selection = new SelectionModel<UsuarioResponse>(allowMultiSelect, initialSelection);
  selectedUser: UsuarioResponse;
  updatedUser: UsuarioResponse;
  nothingChanged = false;

  profiles: PerfilResponse[] = [
    {
      nombre: 'Administrador',
      id_perfil: 10
    },
    {
      nombre: 'Director',
      id_perfil: 20
    },
    {
      nombre: 'Administrativo',
      id_perfil: 30
    },
    {
      nombre: 'Visitante',
      id_perfil: 40
    }
  ];

  searchUserForm = this._formBuilder.group({
      nombre_usuario: ['',
        [
          Validators.minLength(4)
        ]
      ],
      dni_persona: ['',
        [
          Validators.pattern('^[0-9]*$'),
        ]
      ],
    }
  );

  updateForm = this._formBuilder.group({
      nombre_usuario: ['',
        [
          Validators.required,
          Validators.minLength(4)
        ]
      ],
      id_perfil: ['',
        [Validators.required]
      ]
    },
  );

  constructor(private _formBuilder: FormBuilder,
              private _userService: UserService,
              private _router: Router,
              public _formUtils: FormUtilsService) {
  }

  ngOnInit(): void {
  }

  searchUser(): void {

    this.resetData();
    this._userService.searchUser(new UsuarioRequest(this.searchUserForm.value))
      .subscribe(response => {
        this.foundUsers.push(...response);
        this.userList = this.foundUsers;
      });
  }

  private resetData(): void {

    this.updatedUser = null;
    this.selectedUser = null;
    this.nothingChanged = false;
    this.resetTable();
  }

  selectUser(): void {

    if (this.selection.selected != null && this.selection.selected.length > 0) {
      this.selectedUser = this.selection.selected[0];
      this.updateForm.controls.id_perfil.setValue(this.selectedUser.id_perfil);
      this.updateForm.controls.nombre_usuario.setValue(this.selectedUser.nombre_usuario);
    }
  }

  resetTable(): void {
    this.userList = null;
    this.foundUsers = [];
  }

  updateUser(): void {

    if (this.anyChange()) {
      this._userService.updateUser(this.updateForm.value, this.selectedUser.id_usuario)
        .then(response => {
          this.updatedUser = this.selectedUser;
          for (const key in this.updateForm.controls) {
            if (this.updatedUser.hasOwnProperty(key)) {
              this.updatedUser[key] = this.updateForm.value[key];
            }
          }
        });
    } else {
      this.nothingChanged = true;
    }
  }

  anyChange(): boolean {
    for (const key in this.updateForm.controls) {
      if (this.selectedUser.hasOwnProperty(key)) {
        if (this.selectedUser[key] !== this.updateForm.value[key]) {
          return true;
        }
      }
    }
    return false;
  }

  selectProfile(profileId): void {
    this.updateForm.controls.id_perfil.setValue(profileId.target.value);
  }
}
