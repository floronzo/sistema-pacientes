import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {UserService} from '../../user.service';
import {Router} from '@angular/router';
import {UsuarioRequest} from '../../usuario-request';
import {UsuarioResponse} from '../../usuario-response';
import {FormUtilsService} from '../../utils/form-utils.service';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-buscar-usuarios',
  templateUrl: './buscar-usuarios.component.html',
  styleUrls: ['./buscar-usuarios.component.css']
})
export class BuscarUsuariosComponent implements OnInit {

  foundUsers: UsuarioResponse[] = [];
  displayedColumns = ['nombre_usuario', 'dni_persona', 'id_perfil', 'telefono', 'mail'];
  userList: UsuarioResponse[];

  searchUserForm = this._formBuilder.group({
      nombre_usuario: ['',
        [
          Validators.minLength(4)
        ]
      ],
      dni_persona: ['',
        [
          Validators.pattern('^[0-9]*$'),
        ]
      ],
    }
  );

  constructor(private _formBuilder: FormBuilder,
              private _userService: UserService,
              private _router: Router,
              public _formUtils: FormUtilsService) {
  }

  ngOnInit(): void {
  }

  searchUser(): void {

    this.foundUsers = [];
    this._userService.searchUser(new UsuarioRequest(this.searchUserForm.value))
      .subscribe(response => {
        this.foundUsers.push(...response);
        this.userList = this.foundUsers;
      });
  }

}
