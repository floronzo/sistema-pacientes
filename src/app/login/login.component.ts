import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {UserService} from '../user.service';
import {UsuarioRequest} from '../usuario-request';
import {UsuarioResponse} from '../usuario-response';
import {FormBuilder, Validators} from '@angular/forms';
import {equalValueValidator} from '../utils/equal-value-validator';
import {Router} from '@angular/router';
import {FormUtilsService} from '../utils/form-utils.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public user: UsuarioResponse;
  errorResponse = false;
  successResponse = true;

  loginForm = this._formBuilder.group({
      nombre_usuario: ['',
        [
          Validators.required,
          Validators.minLength(4)
        ]
      ],
      clave_usuario: ['',
        [
          Validators.required,
          Validators.minLength(4)
        ]
      ]
    });

  constructor(private _userService: UserService,
              private _formBuilder: FormBuilder,
              private _router: Router,
              public _formUtils: FormUtilsService) {
  }

  ngOnInit(): void {
  }

  logUser(): void {
    if (this.loginForm.valid && this.loginForm.touched) {
      this._userService.findUser(new UsuarioRequest(this.loginForm.value))
        .subscribe(response => {
          if (response.dni_persona !== -1) {
            this.successResponse = true;
            this._router.navigateByUrl('/landing');
          } else {
            this.errorResponse = true;
          }
        });
    }
  }

  resetFormError(): void {
    this.errorResponse = false;
  }
}
