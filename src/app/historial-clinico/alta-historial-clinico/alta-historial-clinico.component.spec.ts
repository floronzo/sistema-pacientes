import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltaHistorialClinicoComponent } from './alta-historial-clinico.component';

describe('AltaHistorialClinicoComponent', () => {
  let component: AltaHistorialClinicoComponent;
  let fixture: ComponentFixture<AltaHistorialClinicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltaHistorialClinicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltaHistorialClinicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
