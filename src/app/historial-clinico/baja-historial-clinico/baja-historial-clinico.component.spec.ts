import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BajaHistorialClinicoComponent } from './baja-historial-clinico.component';

describe('BajaHistorialClinicoComponent', () => {
  let component: BajaHistorialClinicoComponent;
  let fixture: ComponentFixture<BajaHistorialClinicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BajaHistorialClinicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BajaHistorialClinicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
