import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BajaHistorialClinicoComponent} from './baja-historial-clinico/baja-historial-clinico.component';
import {ModificacionHistorialClinicoComponent} from './modificacion-historial-clinico/modificacion-historial-clinico.component';
import {AltaHistorialClinicoComponent} from './alta-historial-clinico/alta-historial-clinico.component';
import {BuscarHistorialClinicoComponent} from './buscar-historial-clinico/buscar-historial-clinico.component';



@NgModule({
  declarations: [
    AltaHistorialClinicoComponent,
    BajaHistorialClinicoComponent,
    BuscarHistorialClinicoComponent,
    ModificacionHistorialClinicoComponent,
  ],
  exports: [
    AltaHistorialClinicoComponent,
    BajaHistorialClinicoComponent,
    BuscarHistorialClinicoComponent,
    ModificacionHistorialClinicoComponent,
  ],
  imports: [
    CommonModule
  ]
})
export class HistorialClinicoModule { }
