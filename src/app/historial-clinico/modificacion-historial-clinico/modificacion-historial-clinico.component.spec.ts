import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificacionHistorialClinicoComponent } from './modificacion-historial-clinico.component';

describe('ModificacionHistorialClinicoComponent', () => {
  let component: ModificacionHistorialClinicoComponent;
  let fixture: ComponentFixture<ModificacionHistorialClinicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModificacionHistorialClinicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificacionHistorialClinicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
