import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuscarHistorialClinicoComponent } from './buscar-historial-clinico.component';

describe('BuscarHistorialClinicoComponent', () => {
  let component: BuscarHistorialClinicoComponent;
  let fixture: ComponentFixture<BuscarHistorialClinicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuscarHistorialClinicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuscarHistorialClinicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
