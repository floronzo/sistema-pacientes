import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-historial-clinico',
  templateUrl: './historial-clinico.component.html',
  styleUrls: ['./historial-clinico.component.css']
})
export class HistorialClinicoComponent implements OnInit {

  selectedOption: string;
  availableOptions: string[] = ['alta', 'baja', 'modificación', 'buscar'];

  constructor() { }

  ngOnInit(): void {
  }

  selectOption(option: string): void {
    this.selectedOption = option;
  }

}
