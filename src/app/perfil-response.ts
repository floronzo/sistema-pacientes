export class PerfilResponse {
  nombre: string;
  id_perfil: number;
  permiso?: string;

  constructor(init?: Partial<PerfilResponse>) {
    Object.assign(this, init);
  }
}
