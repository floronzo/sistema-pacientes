import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {equalValueValidator} from '../utils/equal-value-validator';
import {UserService} from '../user.service';
import {UsuarioRequest} from '../usuario-request';
import {UsuarioResponse} from '../usuario-response';
import {Router} from '@angular/router';
import {FormUtilsService} from '../utils/form-utils.service';
import {CiudadResponse} from '../ciudad-response';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit, OnDestroy {

  user: UsuarioResponse;

  cities: CiudadResponse[] = [
    {
      nombre_ciudad: 'Rawson',
      id_ciudad: 100
    },
    {
      nombre_ciudad: 'Trelew',
      id_ciudad: 200
    },
    {
      nombre_ciudad: 'Puerto Madryn',
      id_ciudad: 300
    },
    {
      nombre_ciudad: 'Comodoro Rivadavia',
      id_ciudad: 400
    }
  ];

  registrationForm = this._formBuilder.group({
      nombre_usuario: ['',
        [
          Validators.required,
          Validators.minLength(4)
        ]
      ],
      clave_usuario: ['',
        [
          Validators.required,
          Validators.minLength(4)
        ]
      ],
      confirm_password: ['', Validators.required],
      dni_persona: ['',
        [
          Validators.required,
          Validators.pattern('^[0-9]*$'),
          Validators.minLength(4)
        ]
      ],
      id_perfil: ['',
        [Validators.required]
      ],
      id_ciudad: ['',
        [Validators.required]
      ],
      nombre: ['',
        [Validators.required]
      ],
      apellido: ['',
        [Validators.required]
      ],
      direccion: ['',
        [Validators.required]
      ],
      mail: ['', [
        Validators.required,
        Validators.pattern('^.+@.+$')]
      ],
      telefono: ['',
        [
          Validators.required,
          Validators.minLength(8)
        ]
      ],
      fecha_nac: ['',
        [
          Validators.required,
          Validators.pattern(/^\d{2}\/\d{2}\/\d{4}$/)
        ]
      ],
    },
    {validator: equalValueValidator('confirm_password', 'clave_usuario')}
  );

  constructor(private _formBuilder: FormBuilder,
              private _userService: UserService,
              private _router: Router,
              public _formUtils: FormUtilsService) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  registerUser(): void {
    if (this.registrationForm.valid && this.registrationForm.touched) {
      this._userService.registerUser(new UsuarioRequest(this.registrationForm.value))
        .then(res => this._router.navigateByUrl('/landing'));
    }
  }

  selectCity(cityId): void {
    this.registrationForm.controls.id_perfil.setValue(cityId.target.value);
  }

}
