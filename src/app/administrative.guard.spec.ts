import { TestBed } from '@angular/core/testing';

import { AdministrativeGuard } from './administrative.guard';

describe('AdministrativeGuard', () => {
  let guard: AdministrativeGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(AdministrativeGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
