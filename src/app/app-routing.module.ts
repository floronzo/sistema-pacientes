import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PacientesComponent} from './pacientes/pacientes.component';
import {LoginComponent} from './login/login.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {UsuariosComponent} from './usuarios/usuarios.component';
import {HistorialClinicoComponent} from './historial-clinico/historial-clinico.component';
import {RegistrationComponent} from './registration/registration.component';
import {LandingComponent} from './landing/landing.component';
import {AuthGuard} from './auth.guard';
import {AdministrativeGuard} from './administrative.guard';
import {ForbiddenComponent} from './forbidden/forbidden.component';

const routes: Routes = [
  { path: '', component: LoginComponent, pathMatch: 'full' },
  { path: 'pacientes', component: PacientesComponent, canActivate: [AuthGuard, AdministrativeGuard] },
  { path: 'usuarios', component: UsuariosComponent, canActivate: [AuthGuard, AdministrativeGuard] },
  { path: 'historial-clinico', component: HistorialClinicoComponent, canActivate: [AuthGuard, AdministrativeGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'registration', component: RegistrationComponent },
  { path: 'landing', component: LandingComponent, canActivate: [AuthGuard] },
  { path: 'forbidden', component: ForbiddenComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
