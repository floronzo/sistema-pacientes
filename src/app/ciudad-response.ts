export class CiudadResponse {
  nombre_ciudad: string;
  id_ciudad: number;

  constructor(init?: Partial<CiudadResponse>) {
    Object.assign(this, init);
  }
}
