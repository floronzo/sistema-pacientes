import { Injectable } from '@angular/core';
import {FormGroup} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class FormUtilsService {

  requiredFieldText = 'Campo requerido';

  constructor() { }

  isEmpty(field: string, form: FormGroup): boolean {
    return form.get(field).touched
      && form.get(field).value === '';
  }

  hasError(field: string, form: FormGroup): boolean {
    return form.get(field).invalid
      && form.get(field).touched
      && form.get(field).value !== '';
  }
}
