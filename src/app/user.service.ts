import {Injectable} from '@angular/core';
import {UsuarioRequest} from './usuario-request';
import {AngularFirestore} from '@angular/fire/firestore';
import {combineLatest, forkJoin, Observable, Subject} from 'rxjs';
import {UsuarioResponse} from './usuario-response';
import {filter, map, mergeAll, startWith, take, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  usuario: UsuarioResponse;
  usuarioSubject: Subject<UsuarioResponse> = new Subject<UsuarioResponse>();

  constructor(private _database: AngularFirestore) {
    this.usuarioSubject
      .subscribe(usuario => {
        this.usuario = usuario;
      });
  }

  registerUser(user: UsuarioRequest): Promise<Observable<unknown[]>> {

    user.id_perfil = 40;
    return this._database.collection('/usuario')
      .add({...user})
      .then(ref => ref.set({id_usuario: ref.id}, {merge: true}))
      .then(response => this.usuarioSubject.next(new UsuarioResponse(user))
        , err => err);
  }

  findUser(user: UsuarioRequest): Observable<UsuarioResponse> {

    return this._database.collection('/usuario', ref =>
      ref.where('nombre_usuario', '==', user.nombre_usuario)
        .where('clave_usuario', '==', user.clave_usuario))
      .valueChanges()
      .pipe(filter(response => response.length > 0),
        tap(response => this.usuarioSubject.next(new UsuarioResponse(response[0]))),
        map(response => new UsuarioResponse(response[0])),
        startWith(new UsuarioResponse({dni_persona: -1})));
  }

  searchUser(usuarioRequest: UsuarioRequest): Observable<UsuarioResponse[]> {

    const nameQuery = this._database.collection('/usuario', ref =>
      ref.where('nombre_usuario', '==', usuarioRequest.nombre_usuario))
      .valueChanges()
      .pipe(take(1));
    const dniQuery = this._database.collection('/usuario', ref =>
      ref.where('dni_persona', '==', usuarioRequest.dni_persona))
      .valueChanges()
      .pipe(take(1));

    return forkJoin([nameQuery, dniQuery])
      .pipe(mergeAll());
  }

  deleteUser(data: UsuarioResponse): Promise<any> {
    return this._database
      .collection('/usuario')
      .doc(data.id_usuario)
      .delete();
  }

  updateUser(data: UsuarioResponse, userId: string): Promise<any> {
    return this._database
      .collection('/usuario')
      .doc(userId)
      .set(data, { merge: true });
  }
}
