import {Validators} from '@angular/forms';

export class UsuarioRequest {
  nombre_usuario: string;
  clave_usuario: string;
  dni_persona: number;
  id_perfil: number;
  nombre: string;
  apellido: string;
  direccion: string;
  mail: string;
  telefono: string;
  fecha_nac: string;

  constructor(init?: Partial<UsuarioRequest>) {
    Object.assign(this, init);
  }
}
