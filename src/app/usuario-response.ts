export class UsuarioResponse {
  nombre_usuario?: string;
  telefono?: string;
  mail?: string;
  dni_persona?: number;
  id_perfil?: number;
  id_usuario?: string;

  public constructor(fields?: any) {
    if (fields) {
      this.nombre_usuario = fields.nombre_usuario || this.nombre_usuario;
      this.telefono = fields.telefono || this.telefono;
      this.mail = fields.mail || this.mail;
      this.dni_persona = fields.dni_persona || this.dni_persona;
      this.id_perfil = fields.id_perfil || this.id_perfil;
    }
  }
}
