import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pacientes',
  templateUrl: './pacientes.component.html',
  styleUrls: ['./pacientes.component.css']
})
export class PacientesComponent implements OnInit {

  selectedOption: string;
  availableOptions: string[] = ['alta', 'baja', 'modificación', 'buscar'];

  constructor() { }

  ngOnInit(): void {
  }

  selectOption(option: string): void {
    this.selectedOption = option;
  }

}
