import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificacionPacientesComponent } from './modificacion-pacientes.component';

describe('ModificacionPacientesComponent', () => {
  let component: ModificacionPacientesComponent;
  let fixture: ComponentFixture<ModificacionPacientesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModificacionPacientesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificacionPacientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
