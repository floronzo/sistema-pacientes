import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ModificacionPacientesComponent} from './modificacion-pacientes/modificacion-pacientes.component';
import {BuscarPacientesComponent} from './buscar-pacientes/buscar-pacientes.component';
import {BajaPacientesComponent} from './baja-pacientes/baja-pacientes.component';
import {AltaPacientesComponent} from './alta-pacientes/alta-pacientes.component';



@NgModule({
  declarations: [
    ModificacionPacientesComponent,
    AltaPacientesComponent,
    BajaPacientesComponent,
    BuscarPacientesComponent,
  ],
  exports: [
    ModificacionPacientesComponent,
    AltaPacientesComponent,
    BajaPacientesComponent,
    BuscarPacientesComponent,
  ],
  imports: [
    CommonModule
  ]
})
export class PacientesModule { }
