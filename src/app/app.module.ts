import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {PacientesComponent} from './pacientes/pacientes.component';
import {LoginComponent} from './login/login.component';
import {CarousselComponent} from './caroussel/caroussel.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {UsuariosComponent} from './usuarios/usuarios.component';
import {UsuariosModule} from './usuarios/usuarios.module';
import {HistorialClinicoComponent} from './historial-clinico/historial-clinico.component';
import {HistorialClinicoModule} from './historial-clinico/historial-clinico.module';
import {PacientesModule} from './pacientes/pacientes.module';
import {environment} from '../environments/environment';
import {AngularFirestoreModule} from 'angularfire2/firestore';
import {AngularFireModule} from 'angularfire2';
import {RegistrationComponent} from './registration/registration.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LandingComponent} from './landing/landing.component';
import {ForbiddenComponent} from './forbidden/forbidden.component';
import {GlobalSharedModule} from './global-shared/global-shared.module';

@NgModule({
  declarations: [
    AppComponent,
    PacientesComponent,
    LoginComponent,
    CarousselComponent,
    PageNotFoundComponent,
    UsuariosComponent,
    HistorialClinicoComponent,
    RegistrationComponent,
    LandingComponent,
    ForbiddenComponent,
  ],
  imports: [
    GlobalSharedModule,
    UsuariosModule,
    HistorialClinicoModule,
    PacientesModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
