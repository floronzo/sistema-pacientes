import {Component, OnInit} from '@angular/core';
import {UsuarioResponse} from '../usuario-response';
import {UserService} from '../user.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  public user: UsuarioResponse;

  constructor(public _userService: UserService) {
    this._userService.usuarioSubject
      .subscribe(usuario => {
        this.user = usuario;
      });
  }

  ngOnInit(): void {
    this.user = this._userService.usuario;
  }

}
