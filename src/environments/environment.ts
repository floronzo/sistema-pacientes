// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBbHmQAoNSn_oWMrdT1GHMDmAmMuAWtQnA',
    authDomain: 'sistema-pacientes.firebaseapp.com',
    databaseURL: 'https://sistema-pacientes.firebaseio.com',
    projectId: 'sistema-pacientes',
    storageBucket: 'sistema-pacientes.appspot.com',
    messagingSenderId: '104752779195'
  },
  isAuthGuardEnabled: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
