export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyBbHmQAoNSn_oWMrdT1GHMDmAmMuAWtQnA',
    authDomain: 'sistema-pacientes.firebaseapp.com',
    databaseURL: 'https://sistema-pacientes.firebaseio.com',
    projectId: 'sistema-pacientes',
    storageBucket: 'sistema-pacientes.appspot.com',
    messagingSenderId: '104752779195'
  },
  isAuthGuardEnabled: true
};
